<?php
/**
 * Kindling Assets Actions.
 *
 * @package Kindling_Assets
 */

if (!function_exists('add_action')) {
    return;
}


add_action('kindling_ready', function () {
    /**
     * Theme assets.
    */
    add_action('wp_enqueue_scripts', function () {
        $mainCssPath = apply_filters('kindling_assets_main_css_path', kindling_asset_path('dist/css/main.css'));
        if ($mainCssPath) {
            wp_enqueue_style(
                'kindling/css',
                $mainCssPath,
                [],
                null,
                'all'
            );
        }

        $useJquery = apply_filters('kindling_assets_use_jquery', true);

        // Manifest.js
        $manifestJsPath = apply_filters('kindling_assets_manifest_js_path', kindling_asset_path('dist/js/manifest.js'));
        if ($manifestJsPath) {
            wp_enqueue_script(
                'kindling/manifest/js',
                $manifestJsPath,
                array_filter([
                    $useJquery ? 'jquery' : '',
                ]),
                null,
                true
            );
        }

        // Vendor.js
        $vendorJsPath = apply_filters('kindling_assets_vendor_js_path', kindling_asset_path('dist/js/vendor.js'));
        if ($vendorJsPath) {
            wp_enqueue_script(
                'kindling/vendor/js',
                $vendorJsPath,
                array_filter([
                    $manifestJsPath ? 'kindling/manifest/js' : '',
                ]),
                null,
                true
            );
        }

        // Main.js
        $mainJsPath = apply_filters('kindling_assets_main_js_path', kindling_asset_path('dist/js/main.js'));
        if ($mainJsPath) {
            wp_enqueue_script(
                'kindling/js',
                $mainJsPath,
                array_filter([
                    $vendorJsPath ? 'kindling/vendor/js' : '',
                    $manifestJsPath ? 'kindling/manifest/js' : '',
                ]),
                null,
                true
            );

            $scriptData = apply_filters('kindling_js_script_data', [
                'ajaxUrl' => admin_url('admin-ajax.php'),
            ]);
            if (!empty($scriptData)) {
                wp_localize_script('kindling/js', 'kindling', $scriptData);
            }
        }
    }, 100);

    /**
     * Adds CSS to TinyMCE editor.
    */
    add_action('init', function () {
        // Use main stylesheet for visual editor
        // To add custom override styles edit wp-content/themes/{theme}/assets/sass/layouts/_tinymce.scss
        add_editor_style(kindling_asset_path('dist/css/main.css'));
    });
});

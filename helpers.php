<?php
/**
 * Kindling Assets - Helpers.
 *
 * @package Kindling_Assets
 */

/**
 * Gets a files full asset path.
 *
 * @param  string $path the asset path.
 *
 * @return string
 */
function kindling_asset_path($path)
{
    $path = kindling_asset_versioned('/' . ltrim($path, '/'));

    return network_site_url($path);
}

/**
 * Gets a files path.
 *
 * @param string $path
 * @return string
 */
function kindling_image_path($path)
{
    $path = ltrim($path, '/');
    return network_site_url("/dist/images/{$path}");
}

/**
 * Gets a files vesioned asset path.
 *
 * @param string $path
 * @return string
 */
function kindling_asset_versioned($path)
{
    // Make sure we have a manifest if not just return the path.
    $manifest_path = ABSPATH . '/mix-manifest.json';
    if (!file_exists($manifest_path)) {
        return $path;
    }

    // Check if the path is in the manifest. Use that if it is and the default path if not.
    $manifest = json_decode(file_get_contents($manifest_path), true);

    return isset($manifest[$path]) ? $manifest[$path] : $path;
}
